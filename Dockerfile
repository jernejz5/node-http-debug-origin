FROM node:latest

WORKDIR /usr/src/app

COPY ./package*.json ./

RUN npm install

COPY ./.eslintrc.yml ./
COPY ./server.js ./
COPY ./server-test.js ./

EXPOSE 80

CMD [ "npm", "start" ]