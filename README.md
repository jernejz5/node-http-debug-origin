# node-http-debug-origin

This script can be used to set up HTTP debug origin server on node.

## Usage

This NodeJS server can be used for HTTP debug server. Server responds to all requests with HTTP 200 status codes and HTML description of request in the body.

I am using c9.io provider for this service which is tested in this readme file.

```
➜  Lab curl https://node-http-debug-origin-jernejz5.c9users.io/test -D-
HTTP/1.1 200 OK
x-powered-by: Express
x-reqhdr-user-agent: curl/7.37.1
x-reqhdr-host: node-http-debug-origin-jernejz5.c9users.io
x-reqhdr-accept: */*
x-reqhdr-x-forwarded-proto: https
x-reqhdr-x-forwarded-port: 443
x-reqhdr-x-region: eu
x-reqhdr-x-forwarded-for: 88.221.209.14
x-reqhdr-connection: keep-alive
x-req-id: 63b0c3e0-e15f-4442-afe8-322a4ba161e8
x-req-log: http://node-http-debug-origin-jernejz5.c9users.io/requests/63b0c3e0-e15f-4442-afe8-322a4ba161e8
content-type: text/html; charset=utf-8
content-length: 861
etag: W/"35d-i+g3x2xZEaEI+arvjfduXqhmX58"
date: Tue, 13 Feb 2018 09:17:11 GMT
X-BACKEND: apps-proxy

<html><head></head><body><pre>{
    "id": "63b0c3e0-e15f-4442-afe8-322a4ba161e8",
    "requestLog": "http://node-http-debug-origin-jernejz5.c9users.io/requests/63b0c3e0-e15f-4442-afe8-322a4ba161e8",
    "dateTime": "Tue, 13 Feb 2018 09:17:11 GMT",
    "url": "/test",
    "path": "/test",
    "hostname": "node-http-debug-origin-jernejz5.c9users.io",
    "method": "GET",
    "protocol": "http",
    "remoteIp": "10.240.0.187",
    "cookies": {},
    "query": {},
    "reqHdr": {
        "user-agent": "curl/7.37.1",
        "host": "node-http-debug-origin-jernejz5.c9users.io",
        "accept": "*/*",
        "x-forwarded-proto": "https",
        "x-forwarded-port": "443",
        "x-region": "eu",
        "x-forwarded-for": "88.221.209.14",
        "connection": "keep-alive"
    },
    "resHdr": {},
    "reqBody": "[object Object]"
}</pre></body></html>%
```

### Returning request headers

Response will contain request headers being returned back to the requestor in a form of headers prepended with "x-reqhdr-". This is usefull for debugging which headers browser send.

```
➜  Lab curl https://node-http-debug-origin-jernejz5.c9users.io/test -D-
HTTP/1.1 200 OK
x-reqhdr-user-agent: curl/7.37.1
x-reqhdr-host: node-http-debug-origin-jernejz5.c9users.io
x-reqhdr-accept: */*
x-reqhdr-x-forwarded-proto: https
x-reqhdr-x-forwarded-port: 443
x-reqhdr-x-region: eu
x-reqhdr-x-forwarded-for: 88.221.209.14
x-reqhdr-connection: keep-alive
...
```

### Custom response headers

By sending request headers prepended with "x-reshdr-" debug server will respond with specified header. This is usefull for testing browser features based on response headers.

```
➜  curl https://node-http-debug-origin-jernejz5.c9users.io/test  -H "x-reshdr-test-header: testvalue" -D-
HTTP/1.1 200 OK
test-header: testvalue
...
```

### Custom response status code

You can send request header "x-res-code" with status code you would like to be responded back to the client and debug server will set this response code.  

```
➜  Lab curl https://node-http-debug-origin-jernejz5.c9users.io/test  -H "x-res-code: 288" -D-
HTTP/1.1 288 unknown
...
```

### Request logging

Each request is logged on the server. Request log entry is accesible trough url which is passed in x-req-log header.

```
➜  Lab curl https://node-http-debug-origin-jernejz5.c9users.io/test -D-
HTTP/1.1 200 OK
x-req-log: http://node-http-debug-origin-jernejz5.c9users.io/requests/63b0c3e0-e15f-4442-afe8-322a4ba161e8
...
```

Retrieving request log entry:

```
➜  Lab curl http://node-http-debug-origin-jernejz5.c9users.io/requests/63b0c3e0-e15f-4442-afe8-322a4ba161e8
{
  "id": "63b0c3e0-e15f-4442-afe8-322a4ba161e8",
  "requestLog": "http://node-http-debug-origin-jernejz5.c9users.io/requests/63b0c3e0-e15f-4442-afe8-322a4ba161e8",
  "dateTime": "Tue, 13 Feb 2018 09:17:11 GMT",
  "url": "/test",
  "path": "/test",
  "hostname": "node-http-debug-origin-jernejz5.c9users.io",
  "method": "GET",
  "protocol": "http",
  "remoteIp": "10.240.0.187",
  "cookies": {},
  "query": {},
  "reqHdr": {
    "user-agent": "curl/7.37.1",
    "host": "node-http-debug-origin-jernejz5.c9users.io",
    "accept": "*/*",
    "x-forwarded-proto": "https",
    "x-forwarded-port": "443",
    "x-region": "eu",
    "x-forwarded-for": "88.221.209.14",
    "connection": "keep-alive"
  },
  "resHdr": {},
  "reqBody": "[object Object]"
}
```