const express = require('express');
const morgan = require('morgan');
const uuid = require('uuid');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const logger = require('log4js').getLogger('server');
const fs = require('fs');
const path = require('path');
const _ = require('underscore');

const app = express();
app.use((req, res, next) => {
  req.id = uuid.v4();
  next();
});

const logDirectory = path.join(__dirname, '/requests');

if (!fs.existsSync(logDirectory)) {
  fs.mkdirSync(logDirectory);
}

morgan.token('requestId', req => req.id);
morgan.token('requestLog', req => `http://${req.get('host')}/requests/${req.id}`);

const morganLine = '[:date[iso]] :method :status :url :response-time :res[content-length] :requestLog';
app.use(morgan(morganLine));

app.use(bodyParser.text({
  type: '*/*',
}));

app.use(cookieParser());

app.get('/requests/*', (req, res) => {
  const requestId = req.path.substring(10);

  fs.readFile(`./requests/${requestId}.json`, (err, data) => {
    if (err) {
      return logger.error(`Error writing request down: ${err}`);
    }

    return res.set('Content-Type', 'application/json').send(data);
  });
});

app.all('/*', (req, res) => {
  const dbg = {};

  req.debug = dbg;

  if (req.get('debug') || req.query.debug || req.cookies.debug) {
    req.debug = dbg;
  }

  // Capture additional details
  dbg.id = req.id;
  dbg.dateTime = new Date().toGMTString();
  dbg.url = req.originalUrl;
  dbg.path = req.path;
  dbg.hostname = req.get('host');
  dbg.method = req.method;
  dbg.protocol = req.protocol;
  dbg.remoteIp = req.ip;
  dbg.requestLog = `http://${dbg.hostname}/requests/${dbg.id}`;

  dbg.cookies = req.cookies;
  dbg.query = req.query;

  dbg.reqHdr = {};
  dbg.resHdr = {};

  // Request and response headers
  _.each(_.pairs(req.headers), (pair) => {
    const header = pair[0];
    dbg.reqHdr[header] = req.get(header);
    res.set(`x-reqhdr-${header}`, req.get(header));

    if (header.toLowerCase().lastIndexOf('x-reshdr-', 0) === 0) {
      dbg.resHdr[header.substring(9)] = req.get(header);
      res.set(`x-reshdr-${header.substring(9)}`, req.get(header));
      res.set(header.substring(9), req.get(header));
    }
  });

  // Body
  if (req.body) {
    dbg.reqBody = req.body.toString();
  }

  // Prepare response
  res.set('x-req-id', dbg.id);
  res.set('x-req-log', dbg.requestLog);


  if (req.get('x-res-code')) {
    dbg.resStatusCode = req.get('x-res-code');
    res.status(req.get('x-res-code'));
  }

  if (req.get('x-res-body')) {
    dbg.resStatusCode = req.get('x-res-body');
    res.status(req.get('x-res-body'));
    res.send(req.get('x-res-body'));
  } else {
    res.send(`<html><head></head><body><pre>${JSON.stringify(dbg, null, 4)}</pre></body></html>`);
  }

  // Log full request
  fs.writeFile(`./requests/${dbg.id}.json`, JSON.stringify(dbg, null, 2), (err) => {
    if (err) { logger.error(`Error writing request down: ${err}`); }
  });
});

app.listen(process.env.PORT || '10080', () => {
  logger.info('App server up and running.');
});

module.exports = app;
