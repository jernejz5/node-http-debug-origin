/* eslint-env mocha */

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('./server');

chai.use(chaiHttp);

describe('Basic server for GET requests', () => {
  it('should return response with 200', (done) => {
    chai.request(server)
      .get('/')
      .set('Host', 'test')
      .end((err, res) => {
        chai.expect(res).to.have.status(200);
        done();
      });
  });
});

describe('Basic server for POST requests', () => {
  it('should return response with 200', (done) => {
    chai.request(server)
      .post('/')
      .set('Host', 'test')
      .send('testBody')
      .end((err, res) => {
        chai.expect(res).to.have.status(200);
        done();
      });
  });
});

describe('Returning request headers', () => {
  it('should return response with request user agent', (done) => {
    chai.request(server)
      .get('/')
      .set('Host', 'test')
      .set('User-Agent', 'testAgent')
      .end((err, res) => {
        chai.expect(res).to.have.header('x-reqhdr-user-agent', 'testAgent');
        done();
      });
  });
});

describe('Custom response headers', () => {
  it('should return response with test header', (done) => {
    chai.request(server)
      .get('/')
      .set('Host', 'test')
      .set('x-reshdr-test-header', 'testValue')
      .end((err, res) => {
        chai.expect(res).to.have.header('test-header', 'testValue');
        done();
      });
  });
});

describe('Custom response status code', () => {
  it('should set custom response code 401', (done) => {
    chai.request(server)
      .get('/')
      .set('Host', 'test')
      .set('x-res-code', '401')
      .end((err, res) => {
        chai.expect(res).to.have.status('401');
        done();
      });
  });
  it('should set custom response code 503', (done) => {
    chai.request(server)
      .get('/')
      .set('Host', 'test')
      .set('x-res-code', '503')
      .end((err, res) => {
        chai.expect(res).to.have.status('503');
        done();
      });
  });
});

describe('Custom response body', () => {
  it('should return custom response body', (done) => {
    chai.request(server)
      .get('/')
      .set('Host', 'test')
      .set('x-res-body', 'testtext')
      .set('x-reshdr-Content-Type', 'text/plain')
      .end((err, res) => {
        chai.expect(res).to.have.text('testtext');
        done();
      });
  });
});

describe('Request logging', () => {
  it('should return response with request id', (done) => {
    chai.request(server)
      .get('/')
      .set('Host', 'test')
      .end((err, res) => {
        chai.expect(res).to.have.header('x-req-id', /[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}/);
        done();
      });
  });
  it('should return response with log link', (done) => {
    chai.request(server)
      .get('/')
      .set('Host', 'test.hostname')
      .end((err, res) => {
        chai.expect(res).to.have.header('x-req-log', /^http:\/\/test.hostname\/requests\/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}/);
        done();
      });
  });
});
